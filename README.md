# pygradle-seed project
This is a sample project that demonstrates building python with gradle using pyGradle.
In particular, it demonstrates some of the capability of gradle plugin: [pygradle](https://github.com/linkedin/pygradle).

### Prerequisites
A fair amount of python stack knowledge is assumed: pytest, flake8, pip, setuptools.  No
gradle knowledge is assumed here.  The only software prerequisite outside of python
dev tools is ```gradle 3```.

## Basic Python Project
Below is the structure of a basic python project.  The manifest, some configuration,
README, setup.py, and source and tests.

```
.
├── MANIFEST.in
├── pytest.ini
├── README.md
├── setup.cfg
├── setup.py
├── src/
└── tests/
```
Out of the box, its ready to run tests.
```
sstump@SPARROW ~/development/pygradle-seed (master) $ pytest
============================= test session starts ==============================
platform linux -- Python 3.5.2, pytest-3.0.5, py-1.4.32, pluggy-0.4.0
cachedir: .cache
rootdir: /home/sstump/development/pygradle-seed, inifile: pytest.ini
collected 6 items

tests/test_.py::TestSuiteA0000::test_000 PASSED
tests/test_.py::TestSuiteA0000::test_001 PASSED
tests/test_.py::TestSuiteA0000::test_002 PASSED
tests/test_.py::TestSuiteA0000::test_003 PASSED
tests/test_.py::TestSuiteA0000::test_004 PASSED
tests/test_.py::TestSuiteA0000::test_005 PASSED

=========================== 6 passed in 0.01 seconds ===========================
sstump@SPARROW ~/development/pygradle-seed (master) $
```
Using `pygradle-seed`, you get a few additions to the project: some gradle config, and a folder
containing bootstrap scripts.
```
.
├── bootstrap/
├── build.gradle
├── gradle.properties
├── MANIFEST.in
├── pytest.ini
├── README.md
├── settings.gradle
├── setup.cfg
├── setup.py
├── src/
└── tests/
```
todo: introduce the contents of gradle.properties here

## Getting Started with pygradle
First, initialize gradle and build the wrapper:
```
gradle wrapper --gradle-version 3.0
```
Then to start the build:
```
./gradlew build
```
These steps are consolidated in ```bootstrap/init-project.sh```.
Project configuration is in ```gradle.properties``` says:
```
// package name
packageName = application

// version
packageVersion = 0.0.1

```
The tail of the build output is shown below.
```
:
hard linking tests/test_.py -> application-0.0.1/tests
copying setup.cfg -> application-0.0.1
Writing application-0.0.1/setup.cfg
Creating tar archive
removing 'application-0.0.1' (and everything under it)
:assemble
:flake8
:pytest
============================= test session starts ==============================
platform linux -- Python 3.5.2, pytest-2.9.1, py-1.4.29, ...
cachedir: .cache
rootdir: /home/sstump/development/pygradle-seed, inifile: pytest.ini
plugins: cov-2.2.1
collecting ... collected 6 items

tests/test_.py::TestSuiteA0000::test_000 PASSED
tests/test_.py::TestSuiteA0000::test_001 PASSED
tests/test_.py::TestSuiteA0000::test_002 PASSED
tests/test_.py::TestSuiteA0000::test_003 PASSED
tests/test_.py::TestSuiteA0000::test_004 PASSED
tests/test_.py::TestSuiteA0000::test_005 PASSED

=========================== 6 passed in 0.01 seconds ===========================
:check
:build

BUILD SUCCESSFUL

Total time: 58.472 secs
sstump@SPARROW ~/development/pygradle-seed (master) $
```
There's a ```build``` directory, among others.  It looks like this:
```
build
├── distributions
│   └── application-0.0.1.tar.gz
└── venv
    ├── bin
    ├── include
    ├── lib
    └── pip-selfcheck.json
```
There's a source distribution, ```application-0.0.1.tar.gz```, which agrees
with ```gradle.properties```.  But wait, there's more...

### updates
- I've been able to get wheel artifacts
- I have integrated sphinx.autodoc

## pygradle venv
Gradle also built a virtual environment with your code installed and ready
to run.  The virtual environment is available at the project root
by: ```source activate``` (which symlinks to ```build/venv```).  Your code
and dependencies are fully installed and tested.

This project has some entry points defined, so we can activate the environment
and execute the commands.
```
sstump@SPARROW ~/development/pygradle-seed (master) $ source activate
(application)sstump@SPARROW ~/development/pygradle-seed (master) $ appcmd p1 p2 pK
appcmd ['p1', 'p2', 'pK']
(application)sstump@SPARROW ~/development/pygradle-seed (master) $ appcmd.a p1 p2 pK
appcmd.a: created Gazelle named grandpa ['p1', 'p2', 'pK']
(application)sstump@SPARROW ~/development/pygradle-seed (master) $ appcmd.b p1 p2 pK
appcmd.b: created AmericanBison named galaxy ['p1', 'p2', 'pK']
(application)sstump@SPARROW ~/development/pygradle-seed (master) $ appcmd.c p1 p2 pK
appcmd.c: created BlueCrab named m-prd-ext0003 ['p1', 'p2', 'pK']
(application)sstump@SPARROW ~/development/pygradle-seed (master) $ appcmd.d p1 p2 pK
appcmd.d: created Canvasback named daffy ['p1', 'p2', 'pK']
(application)sstump@SPARROW ~/development/pygradle-seed (master) $
```
TODO:
=====

- sphinx autodoc, like [this](https://bitbucket.org/samstump/combalg-py).
- document the wheel and doc artifacts [here](https://bitbucket.org/samstump/combalg-py/src/a1cd91591691d83a4a1f9135c9426526e62717c9/build.gradle?at=master&fileviewer=file-view-default).