# app_class.py


class Application:
    @staticmethod
    def help(topic=None):
        if topic is None:
            return 'Application.help'
        else:
            return 'Application.help.{}'.format(topic)
