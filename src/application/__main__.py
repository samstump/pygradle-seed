import sys

from .app_class import Application
from .system_one.antelope import Gazelle
from .system_one.buffalo import AmericanBison
from .system_two.crab import BlueCrab
from .system_two.duck import Canvasback


def alternate_entry_point_a(args=None):
    if args is None:
        cmd = sys.argv[0].split('/')[-1]
        args = sys.argv[1:]
    # process this entry point
    x = Gazelle('grandpa')
    print('{}: created {} named {} {}'.format(cmd, type(x).__name__, x.name, str(args)))


def alternate_entry_point_b(args=None):
    if args is None:
        cmd = sys.argv[0].split('/')[-1]
        args = sys.argv[1:]
    # process this entry point
    x = AmericanBison('galaxy')
    print('{}: created {} named {} {}'.format(cmd, type(x).__name__, x.name, str(args)))


def alternate_entry_point_c(args=None):
    if args is None:
        cmd = sys.argv[0].split('/')[-1]
        args = sys.argv[1:]
    # process this entry point
    x = BlueCrab('m-prd-ext0003')
    print('{}: created {} named {} {}'.format(cmd, type(x).__name__, x.name, str(args)))


def alternate_entry_point_d(args=None):
    if args is None:
        cmd = sys.argv[0].split('/')[-1]
        args = sys.argv[1:]
    # process this entry point
    x = Canvasback('daffy')
    print('{}: created {} named {} {}'.format(cmd, type(x).__name__, x.name, str(args)))


def main(args=None):
    if args is None:
        cmd = sys.argv[0].split('/')[-1]
        args = sys.argv[1:]
    if len(args) == 0:
        print(Application().help(str(args)))
    else:
        print('{} {}'.format(cmd, str(args)))


if __name__ == "__main__":
    main()
